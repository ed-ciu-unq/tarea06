# FRANKENSTEIN: Estructuras + Haskell + git

El proyecto Frankenstein consiste en una cantidad de tareas, en orden creciente de dificultad, que cubren el contenido de la parte de Haskell de Estructuras de Datos.

El objetivo principal en Frankenstein consiste en “matar los _undefined_”. Un _undefined_ representa un espacio en blanco que se debe completar con código que cumpla la funcionalidad esperada.

El comando `stack test` permite correr los tests de la tarea. Al principio todos ellos deben estar en rojo. La meta es que todos pasen a estar en verde.

## Tarea 06: Funciones varias sobre listas

Funciones sobre listas y números. Pueden usar recursión o no. Pueden utilizar algunos de los patrones de recursividad vistos, o ninguno de ellos.

A pensar...

---

Autor: Román García (nykros@gmail.com)
