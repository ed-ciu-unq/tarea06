module TareaSpec (spec) where
import Test.Hspec
import Tarea

spec :: Spec
spec =
  describe "Pruebas para la tarea" $ do

    it "tomar" $
      tomar 3 [4,3,5,3,2] `shouldBe` [4,3,5]

    it "dejar" $     
      dejar 3 [4,3,5,3,2] `shouldBe` [3,2]

    it "comienzaCon 1" $   
      comienzaCon "Alla" "Alla vamos" `shouldBe` True

    it "comienzaCon 2" $      
      comienzaCon ""     "Alla vamos" `shouldBe` True

    it "comienzaCon 3" $ 
      comienzaCon "Ala " "Alla vamos" `shouldBe` False

    it "reversa" $     
      reversa [4,3,9,19] `shouldBe` [19,9,3,4]

    it "unir" $     
      unir [1,2,3] [4,5] `shouldBe` [1,2,3,4,5]

    it "concatenar" $     
      concatenar ["to","be","or"] `shouldBe` "tobeor"

    it "replicar" $     
      replicar 4 False `shouldBe` [False,False,False,False]

    it "sinUltimo" $      
      sinUltimo [2,4,5] `shouldBe` [2,4]

    it "funcionX" $      
      funcionX [2,4,5] `shouldBe` (3,[2,4,5])

    it "primeroYUltimo" $      
      primeroYUltimo [2,4,5] `shouldBe` (2,5)
