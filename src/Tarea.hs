module Tarea where


-- Dada una lista, devuelve los primeros n elementos de la lista. {-AKA: take -}
-- Parcial si n<0
tomar :: Int -> [a] -> [a]
tomar = undefined


-- Dada una lista, devueve la lista sin los primeros n elementos. {-AKA: drop -}
dejar :: Int -> [a] -> [a]
dejar = undefined


-- Devuelve true si la segunda lista comienza con los elementos de la primera
comienzaCon :: Eq a => [a] -> [a] -> Bool
comienzaCon = undefined


-- Dada una lista, devuelve la lista al reves. {-AKA: reverse -}
reversa :: [a] -> [a]
reversa = undefined


-- Dadas dos listas, devueve la lista que es la concatenacion de ambas {-AKA: ++ -}
unir :: [a] -> [a] -> [a]
unir = undefined


-- Dada una lista de listas, devuelve la lista con los datos de las sublistas {-AKA: concat -}
concatenar :: [[a]] -> [a]
concatenar = undefined


-- Dado un numero n y un elemento, devuelve una lista con n elementos. {-AKA: replicate -}
replicar ::  Int -> a -> [a]
replicar = undefined


-- Dada una lista, devueve la lista sin el ultimo elemento. {-AKA: init -}
-- Parcial cuando esta vacia
sinUltimo :: [a] -> [a]
sinUltimo = undefined


-- Dada una lista, devolver un par donde la primera componente es la cantidad
-- de elementos de la lista y la segunda componente es la misma lista
funcionX :: [a] -> (Int, [a])
funcionX = undefined


-- Dada una lista con al menos dos elementos,
-- devolver un par con el primero y el ultimo elemento
primeroYUltimo :: [a] -> (a,a)
primeroYUltimo = undefined

